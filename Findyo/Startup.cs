﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Findyo.Contexts;
using Findyo.IContexts;
using Findyo.IRepositories;
using Findyo.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Findyo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.Configure<Settings>(
                    options =>
                    {
                        options.ConnectionString = Configuration.GetSection("MongoDB:ConnectionString").Value;
                        options.Database = Configuration.GetSection("MongoDB:Database").Value;
                    });
            services.AddTransient<IFindyo_job_CategoriesContext, Findyo_job_Categories>();
            services.AddTransient<IJob_Category_Repository, Job_Category_Repository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
