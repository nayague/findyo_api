﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Findyo.IRepositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Findyo.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class Job_CategoryController : ControllerBase
    {
        private readonly IJob_Category_Repository _Job_Category_Repository;
        public Job_CategoryController(IJob_Category_Repository Job_Category_Repository)
        {
            _Job_Category_Repository = Job_Category_Repository;
        }

        // GET: api/Job_Category
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return new ObjectResult(await _Job_Category_Repository.GetAllJob_Categories());
        }

        // GET: api/Job_Category/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Job_Category
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Job_Category/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
