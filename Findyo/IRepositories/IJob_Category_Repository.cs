﻿using Findyo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Findyo.IRepositories
{
    public interface IJob_Category_Repository
    {
        Task<IEnumerable<Job_Category>> GetAllJob_Categories();
    }
}
