﻿using Findyo.IContexts;
using Findyo.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Findyo.Contexts
{
    public class Findyo_job_Categories : IFindyo_job_CategoriesContext
    {
        private readonly IMongoDatabase _db;
        public Findyo_job_Categories(IOptions<Settings> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            _db = client.GetDatabase(options.Value.Database);
        }
        public IMongoCollection<Job_Category> Job_Categories => _db.GetCollection<Job_Category>("findyo_Job_Categories");
    }
}
