﻿using Findyo.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Findyo.IContexts
{
    public interface IFindyo_job_CategoriesContext
    {   
        IMongoCollection<Job_Category> Job_Categories { get; }
    }
}
