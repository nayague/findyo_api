﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Findyo.Models
{
    public class Job_Category
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        
    }
}
