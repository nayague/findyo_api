﻿using Findyo.IContexts;
using Findyo.IRepositories;
using Findyo.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Findyo.Repositories
{
    public class Job_Category_Repository : IJob_Category_Repository
    {
        private readonly IFindyo_job_CategoriesContext _context;
        public Job_Category_Repository(IFindyo_job_CategoriesContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Job_Category>> GetAllJob_Categories()
        {
            return await _context
            .Job_Categories
            .Find(_ => true)
            .Sort("{Name:1}")
            .ToListAsync();
        }
    }
}
